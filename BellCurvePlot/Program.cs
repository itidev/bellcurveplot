﻿using ScottPlot;
using System.Drawing;

var plt = new Plot(1000, 400);

var median = 200000;
var low = 100000;
var high = 300000;
var yourHouse = 235000;

var eightiethPercentile = 0.84;
var endX = 2;
var step = 0.01;

// real standard deviation -> (80th percentile - median) / 0.84
var yourHouseX = (yourHouse - median) / ((high - median) / eightiethPercentile);

var xs = GetRange(-endX, endX, step);
var first20PercentX = GetRange(-endX, -eightiethPercentile, step);
var last20PercentX = GetRange(eightiethPercentile, endX, step);

var yList = new List<double>();
var first20PercentYList = new List<double>();
var last20PercentYList = new List<double>();

foreach (var x in xs)
{
    var y = StandardNormalDistribution(x);
    yList.Add(y);
}

foreach (var x in first20PercentX)
{
    var y = StandardNormalDistribution(x);
    first20PercentYList.Add(y);
}

foreach (var x in last20PercentX)
{
    var y = StandardNormalDistribution(x);
    last20PercentYList.Add(y);
}

var ys = yList.ToArray();
var first20PercentY = first20PercentYList.ToArray();
var last20PercentY = last20PercentYList.ToArray();

plt.AddScatterLines(xs, ys);

var medianLine = plt.AddVerticalLine(0, Color.Gray, 2);
medianLine.Max = 0.45;
var medianLabel = plt.AddText($"Median: {GetCostLabel(median)}", 0, 0.48, color: Color.Black);
medianLabel.Alignment = Alignment.UpperCenter;

var lowLine = plt.AddVerticalLine(-eightiethPercentile, Color.Gray, 2);
lowLine.Max = 0.35;
var lowLabel = plt.AddText($"Low: {GetCostLabel(low)}", -eightiethPercentile, 0.38, color: Color.Black);
lowLabel.Alignment = Alignment.UpperCenter;

var highLine = plt.AddVerticalLine(eightiethPercentile, Color.Gray, 2);
highLine.Max = 0.35;
var highLabel = plt.AddText($"High: {GetCostLabel(high)}", eightiethPercentile, 0.38, color: Color.Black);
highLabel.Alignment = Alignment.UpperCenter;

var yourHouseLine = plt.AddVerticalLine(yourHouseX, Color.CornflowerBlue, 2);
yourHouseLine.Max = 0.5;
var yourHouseLabel = plt.AddText($"Your house: {GetCostLabel(yourHouse)}", yourHouseX, 0.53, color: Color.CornflowerBlue);
yourHouseLabel.Alignment = Alignment.UpperCenter;

var green = Color.LightGreen;
var transparentGreen = Color.FromArgb(127, green.R, green.G, green.B);

var wholeCurveFill = plt.AddFill(xs, ys, color: transparentGreen);
wholeCurveFill.LineWidth = 0;

var first20PercentFill = plt.AddFill(first20PercentX, first20PercentY, color: transparentGreen);
first20PercentFill.LineWidth = 0;

var last20PercentFill = plt.AddFill(last20PercentX, last20PercentY, color: transparentGreen);
last20PercentFill.LineWidth = 0;

plt.SetAxisLimits(yMin: 0);
plt.Frameless();
plt.Grid(false);

plt.SaveFig("bellCurvePlot.png");

static double StandardNormalDistribution(double x)
{
    return Math.Exp(-(x * x) / 2) / Math.Sqrt(2 * Math.PI);
}

static string GetCostLabel(int cost)
{
    var nearestThousand = Math.Round(cost / 1000d) * 1000;

    if (nearestThousand >= 1000000)
    {
        var shortMillion = nearestThousand / 1000000d;
        return $"{shortMillion}M";
    }

    if (cost >= 1000)
    {
        var shortThousand = nearestThousand / 1000d;
        return $"{shortThousand}K";
    }

    return cost.ToString();
}

static double[] GetRange(double start, double stop, double step)
{
    var range = new List<double>();

    for (var i = start; i <= stop; i += step)
    {
        range.Add(i);
    }

    return range.ToArray();
}
